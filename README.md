# **Plot Ns distribution**

### Ns distribution for a given genome
plot using plotKariotype bioconductor package
only the genome fasta file is required has input

Demo of the package karyoploteR:
[karyoploteR](https://bioconductor.org/packages/release/bioc/vignettes/karyoploteR/inst/doc/karyoploteR.html)

!!! Not optimized: needs about 5mn for a 400Mb genome

### usage:
`./run_Ns_distribution.sh <mygenpme.fasta>`


2 steps: (2 scripts):
#### 1) Ns_distribution4chr_bands_and_density.py

Ns distribution for a given genome
generate the tables used by the R script: plotideotable_Ns_bands_and_density_plotKariotype_4args.R
requirement: python3 and biopyton

inputs:
     genome fasta file
     output prefix

outputs:
Generate tables from a genome in fasta format
1) prefix.chrs.csv
   table with chrs list: custom input genome for the plotKariotype R package
2) csv with Ns count (>50 Ns ) used 4 the Kariotype bands (in red)
3) csv sliding windows with Ns count >10 and [ATGC]{6,}

usage:
`python3 Ns_distribution4chr_bands_and_density.py mygenome.fasta mygenome`


#### 2)plotideotable_Ns_bands_and_density_plotKariotype_4args.R
to plot the Ns distribution karyoploteR


Jacques Lagnel
INRAe, GAFL

