from Bio import SeqIO
from numpy import arange
import re
import sys

#INRAe, GAFL,jacques 2021

'''
Ns distribution for a given genome
generate the tables used by the R script: plotideotable_Ns_bands_and_density_plotKariotype_4args.R
requirement: python3 and biopyton

inputs:
     genome fasta file
     output prefix

outputs:
Generate tables from a genome in fasta format
1) prefix.chrs.csv
   table with chrs list: custom input genome for the plotKariotype R package
2) csv with Ns count (>50 Ns ) used 4 the Kariotype bands (in red)
3) csv sliding windows with Ns count >10 and [ATGC] >6

usage:
python3 Ns_distribution4chr_bands_and_density.py mygenome.fasta mygenome

'''


# Returns a generator that will iterate through
def slidingWindow(sequence,winSize,step):
    # Pre-compute number of chunks to emit
    numOfChunks = ((len(sequence)-winSize)/step)+1
    # build the chuncks
    for i in range(0,int(numOfChunks*step),step):
        yield sequence[i:i+winSize]



def countNsByBin(fastaf,mybin,prefix,winSize,step):
    s=0
    e=mybin

    chrs = open("{}.chrs.csv".format(prefix), "w")
    bands= open("{}.bands.csv".format(prefix), "w")

    chrs.write("chr\tstart\tend\tname\tgieStain\n")
    bands.write("chr\tstart\tend\tname\tgieStain\n")

    dens = open("{}.density.csv".format(prefix), "w")
    dens.write("chr\tstart\tend\twidth\n")

    #chr start end count Ns
    with open(fastaf,'r') as fh:
        for record in SeqIO.parse(fh, "fasta"):
            chrpos=0
            chrstart=0
            ncount=0
            bandcnt=1
            chrl=len(record.seq)
            sdef=record.id.replace("Super-Scaffold_",'')
            print("chr:{}\t({}bp)".format(sdef,chrl), flush=True)
            chrs.write ("{}\t1\t{}\tchr\tgneg\n".format(sdef,chrl))
            bands.write("{}\t1\t{}\tchr\tgneg\n".format(sdef,chrl))
            print ("building Bands for chr:{}".format(sdef), flush=True)
            while chrpos<chrl:
                ncount=0
                chrstart=chrpos+1 # 0 based
                if record.seq[chrpos] =='N': #record.seq[chrpos:chrpos+1] =='N': #record.seq.upper().count('N', chrpos, chrpos) >0: #aseq[chrpos].upper() =='N':
                    while chrpos<chrl:
                        if record.seq[chrpos] =='N': #aseq[chrpos].upper() =='N':
                           ncount=ncount+1
                        elif record.seq.count('N', chrpos, chrpos+10) >1:
                           ncount=ncount+record.seq.count('N', chrpos, chrpos+10)
                           chrpos=chrpos+9
                        else:
                           break
                        chrpos=chrpos+1

                if ncount>=50:
                    #print("{}\t{}\t{}\tband{}\tacen\tsg={}".format(sdef,chrstart,chrpos,bandcnt,ncount),flush=True)
                    bands.write("{}\t{}\t{}\tband{}\tacen\n".format(sdef,chrstart,chrpos,bandcnt))
                    bandcnt=bandcnt+1
                chrpos=chrpos+1

            '''
            generate the csv file with Ns count in sliding windows
            prefix.density.csv
            chr start end width
            '''
            bandcnt=1
            chrstart=1
            Ns=0
            print ("building density for chr:{}".format(sdef), flush=True)
            chunks = slidingWindow(record.seq,winSize,step)
            for chunk in chunks:
                Ns=chunk.upper().count('N')
                if Ns>10:
                   dens.write("{}\t{}\t{}\t{}\n".format(sdef,chrstart,chrstart+winSize,Ns))
                chrstart=chrstart+step

    bands.close()
    chrs.close()
    dens.close()

#  python3 Ns_distribution4chr_bands_and_density.py mygenome.fasta mygenome
if __name__ == '__main__':
    args = sys.argv[1:]
    if args == []:
        args = ['-']

    fastaf=args[0]   # genome fasta file
    mybin=1000       # bins intervals in bp
    prefix=args[1]   # output files prefix
    winSize=10000    # window size 4 the sliding window
    step=1000        # step size 4 the sliding window
    countNsByBin(fastaf,mybin,prefix,winSize,step)
    #countNsByBin(args[0],1000,args[1])
    print ()

