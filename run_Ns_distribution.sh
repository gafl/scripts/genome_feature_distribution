#!/bin/bash
#SBATCH --job-name=Nsdistribution # job name (-J)
#SBATCH --time="00:15:00" #max run time "hh:mm:ss" or "dd-hh:mm:ss" (-t)
#SBATCH --cpus-per-task=1 # max nb of cores (-c)
#SBATCH --ntasks=1 #nb of tasks
#SBATCH --mem=4G # max memory (-m)
#SBATCH --output=Nsdistribution.out #stdout (-o)

########################## On genotoul ###############################
# Uncomment the module load for genotoul
## for singularity
#module load system/singularity-3.5.3
######################################################################
export SINGULARITY_BINDPATH="/work2/project/gafl"

# https://bioconductor.org/packages/devel/bioc/manuals/karyoploteR/man/karyoploteR.pdf
# https://bernatgel.github.io/karyoploter_tutorial/

fasta=$1  #"./c9_smartdenovo_pilon2_hybrid_scaffold_NoBuchnera.fasta"
#pref="c9_smartdenovo_pilon2_hybrid_scaffold_NoBuchnera"

base=${fasta%.fa*}
pref=${base##*/}

echo $pref

/work2/project/gafl/tools/containers/biopython_v1.70.sif Ns_distribution4chr_bands_and_density.py $fasta ${pref}

singularity exec /work2/project/gafl/tools/containers/R_base_V3.6.0.sif Rscript plotideotable_Ns_bands_and_density_plotKariotype_4args.R ${pref}.chrs.csv ${pref}.bands.csv ${pref}.density.csv ${pref}.pdf

